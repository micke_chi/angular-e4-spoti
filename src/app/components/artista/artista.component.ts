import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import {SpotifyService} from "../../services/spotify.service";

@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
  styleUrls: ['./artista.component.scss']
})
export class ArtistaComponent implements OnInit {

  artist: any = {};
  topTracks: any[] = [];
  loading: boolean;

  constructor(private  router: ActivatedRoute, private spotifyService: SpotifyService) {
    this.loading = true;
    this.router.params.subscribe(params =>{
      console.log("id del artitsa en artista", params['id']);
      this.getArtist(params['id']);
      this.getTopTracks(params['id']);
    });
  }

  ngOnInit() {
  }

  getArtist(id: string){
    this.loading = true;
    this.spotifyService.getArtist(id).subscribe((response: any)=>{
      console.log("informacion del artista", response);
      this.artist = response;
      this.loading = false;
    })
  }

  getTopTracks(id: string){
    this.loading = true;
    this.spotifyService.getTopTracks(id).subscribe((response: any)=>{
      console.log("top tracks", response);
      this.topTracks = response;
      this.loading = false;
    })
  }



}
