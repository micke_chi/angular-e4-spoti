import {Component, Input, OnInit} from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() item: any = null;
  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  showArtist(item:any){
    let artistId = (item.type === 'artist') ? item.id : item.artists[0].id;
    console.log("id artist", artistId);
    this.router.navigate(['/artist', artistId]);

  }

}
