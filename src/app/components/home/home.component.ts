import { Component, OnInit } from '@angular/core';
import {SpotifyService} from "../../services/spotify.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  newReleases: any[] = [];
  loading: boolean;
  error: boolean;

  constructor(private spotifyService: SpotifyService) {

    this.loading = true;
    this.error = false;

    this.spotifyService.getNewReleases().subscribe(
      (response:any)=>{
        console.log("canciones", response);
        this.newReleases = response;
        this.loading = false;
        //this.error = true;
      },
      (errorResponse:any)=>{
        console.log("erro canciones", errorResponse);
        this.loading = false;
        this.error = true;
      });
  }

  ngOnInit() {
  }

}
