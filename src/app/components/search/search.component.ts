import { Component, OnInit } from '@angular/core';
import {SpotifyService} from "../../services/spotify.service";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  artists: any[] = [];
  constructor(private spotifyService: SpotifyService) { }
  loading: boolean;

  ngOnInit() {
  }

  search(termino: string) {

    this.loading = true;
    this.spotifyService.getArtists(termino).subscribe((response:any)=>{
      console.log("Resultados de busqueda", response);
      this.artists = response;
      this.loading = false;
    })
  }

}
