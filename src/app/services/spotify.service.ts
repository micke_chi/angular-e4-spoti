import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor(private http: HttpClient) { }

  getQuery(query: string){
    const url = `https://api.spotify.com/v1/${ query }`;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQBgxctq3YwvD0HaS5PKEvH9tpjfqi_g8K0Nawru1HPPWokcbs9CzQohq3YNS40dNXF-Nj6ANc2fYLUyLtA'
    });

    return this.http.get(url, {headers});
  }

  getNewReleases(){
    return this.getQuery('browse/new-releases?limit=20')
      .pipe(map( response =>{
      return response['albums'].items;
    }));
  }

  getArtists(termino: string){
    return this.getQuery(`search?query=${ termino }&type=artist&limit=15`)
      .pipe(map( response =>{
        return response['artists'].items;
      }));
  }

  getArtist(id: string){
    return this.getQuery(`artists/${id}`);
  }

  getTopTracks(id: string){
    return this.getQuery(`artists/${id}/top-tracks?country=us`)
      .pipe(map(response => response['tracks']));
  }

}
